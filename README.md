# webhook_reciever

## Getting started

A very basic CLI receiver for webhooks written in Flask

## Requirements

Satisfy pip or conda requirements listed in either requirements.txt or environment.yml file.

### Conda

```
cd webhook_receiver
conda env create -f environment.yml
```

### Pip

```
cd webhook_receiver
pip install -r requirements.txt
```

## Usage

launch the webhook receiver app with the following command:

```
python webhook_receiver.py [-p PORT ]
```

By default, the app will listen on all IP address configured on your device including 127.0.0.1 on port 8080
Specify a different port number by using the `-p` or `--port` option followed by a valid port number between 1024 and 65535:

### Example
```
$ python webhook_reciever.py -p 9999
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Serving Flask app 'webhook_reciever'
 * Debug mode: off
INFO:werkzeug: * Running on all addresses (0.0.0.0)
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://127.0.0.1:9999
 * Running on http://192.168.8.182:9999 (Press CTRL+C to quit)
^C
```

### Available routes

The following routes are available:

- `/device` with `POST` method
- `/vmcreate` with `PUT` method
- `/deletedev` with `DELETE` method
- `/deletevm` with `DELETE` method
