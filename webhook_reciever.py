# -*- coding: utf-8 -*-
"""This is a very basic CLI receiver for webhooks written in Flask"""

import logging
import argparse

from flask import Flask, request

app = Flask(__name__)
logging.basicConfig(level=logging.INFO)


ROUTES = {
    "/device": {"POST": "Device Webhook received"},
    "/vmcreate": {"PUT": "VM Create Webhook received"},
    "/deletedev": {"DELETE": "Device Delete Webhook received"},
    "/deletevm": {"DELETE": "VM Delete Webhook received"},
}


def handle_webhook(route, method):
    """Returns a message that confirms that the webhook was received"""
    logging.info("%s data from Webhook contains: %s", method, request.json)
    return ROUTES[route][method]


for route, methods in ROUTES.items():
    for method in methods:
        app.add_url_rule(route, view_func=handle_webhook, methods=[method])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="cli webhook receiver useful for testing webhooks"
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        choices=range(1024, 65535),
        default=8080,
        help="port to listen on (default: 8080, any port 1024 - 65535)",
    )
    args = parser.parse_args()
    app.run(host="0.0.0.0", port=args.port)
